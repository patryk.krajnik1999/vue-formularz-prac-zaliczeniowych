import { createRouter, createWebHistory } from "vue-router";
import WorksList from "@/pages/WorksList.vue";
import NotFound from "@/pages/NotFound.vue";
import AddWork from "@/pages/AddWork.vue";
import UserAuth from "@/pages/UserAuth.vue";
import SubmittedWorks from "@/pages/SubmittedWorks.vue";
import store from "@/store/index";

const routes = [
  {
    path: "/",
    redirect: "/works",
  },
  {
    path: "/works",
    component: WorksList,
    meta: { requiresAuth: true },
  },
  {
    path: "/works/:id",
    component: SubmittedWorks,
    props: true,
    meta: { requiresLecturer: true },
  },
  {
    path: "/addWork",
    component: AddWork,
    meta: { requiresLecturer: true },
  },
  {
    path: "/auth",
    component: UserAuth,
    meta: { requiresUnauth: true },
  },
  {
    path: "/:notFound(.*)",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, _, next) => {
  if (to.meta["requiresAuth"] && !store.getters.isAuthenticated) {
    next("/auth");
  } else if (to.meta["requiresUnauth"] && store.getters.isAuthenticated) {
    next("/works");
  } else if (to.meta["requiresLecturer"] && !store.getters.roles.includes("ROLE_LECTURER")) {
    next("works");
  } else {
    next();
  }
});

export default router;
