export default {
  async loadData(context) {
    const response = await fetch(`http://formularz_prac/works`);

    if (!response.ok) {
      throw new Error("Nie udało się pobrać danych.");
    }

    const responseData = await response.json();

    const works = [];
    for (const res in responseData) {
      const work = {
        id: "w" + res,
        uuId: responseData[res]["id"],
        topic: responseData[res]["topic"],
        description: responseData[res]["description"],
        openDate: responseData[res]["openDate"]["date"],
        closeDate: responseData[res]["closeDate"]["date"],
        userUuId: responseData[res]["userId"],
        submits: responseData[res]["submits"],
      };
      console.log(work["submits"]);
      // console.log(work["submits"].some(w => w.userId === 3));
      works.push(work);
    }

    context.commit("setWorks", works);
  },

  async addWork(context, payload) {
    const newRequest = {
      topic: payload.topic,
      description: payload.description,
      openDate: payload.openDate,
      closeDate: payload.closeDate,
      userUuId: payload.uuId,
    };

    const response = await fetch(`http://formularz_prac/addWork`, {
      method: "POST",
      body: JSON.stringify(newRequest),
    });

    if (!response.ok) {
      throw new Error("Błąd podczas wysyłania danych.");
    }

    const responseData = await response.json();

    console.log(responseData);
  },

  async submitWork(context, payload) {
    const newRequest = {
      workId: payload["workId"],
      userId: payload["userId"],
      file: payload["file"],
      dateTime: new Date(),
    };
    const response = await fetch(`http://formularz_prac/uploadFile`, {
      method: "POST",
      body: JSON.stringify(newRequest),
    });

    if (!response.ok) {
      throw new Error(responseData.message);
    }

    const responseData = await response.json();

    console.log(responseData);
  },
};
