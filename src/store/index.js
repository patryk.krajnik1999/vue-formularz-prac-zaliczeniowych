import { createStore } from "vuex";
import authModule from "./auth/index";
import getters from "@/store/getters";
import mutations from "@/store/mutations";
import actions from "@/store/actions";
import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

export default createStore({
  state: {
    works: [],
  },
  getters,
  mutations,
  actions,
  modules: {
    auth: authModule,
  },
  plugins: [vuexLocal.plugin]
});
