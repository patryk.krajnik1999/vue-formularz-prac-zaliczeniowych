export default {
  userId(state) {
    return state.userId;
  },
  // token(state) {
  //   return state.token;
  // },
  isAuthenticated(state) {
    return state.userId != null;
  },
  roles(state) {
    return state.roles;
  },
  didAutoLogout(state) {
    return state.didAutoLogout;
  },
};
