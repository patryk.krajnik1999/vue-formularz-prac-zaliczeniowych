let timer;

export default {
  async login(context, payload) {
    return context.dispatch("auth", {
      ...payload,
      mode: "login",
    });
  },
  async signup(context, payload) {
    return context.dispatch("auth", {
      ...payload,
      mode: "signup",
    });
  },

  async auth(context, payload) {
    let url = `http://formularz_prac/register`;

    if (payload.mode === "login") {
      url = `http://formularz_prac/login`;
    }

    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify({
        email: payload.email,
        password: payload.password,
        role: payload.role,
      }),
    });

    if (!response.ok) {
      throw new Error("Sprawdź wprowadzone dane.");
    }

    const responseData = await response.json();

    if (payload.mode === "login" && !responseData["isValid"]) {
      throw new Error("Nieprawidłowe dane.");
    }

    const expiresIn = 3600000;
    // const expiresIn = 5000;
    const expirationDate = new Date().getTime() + expiresIn;

    localStorage.setItem("tokenExpiration", expirationDate);
    localStorage.setItem("userId", responseData.userId);
    localStorage.setItem("userRoles", responseData.roles);

    timer = setTimeout(function () {
      context.dispatch("autoLogout");
    }, expiresIn);

    context.commit("setUser", {
      userId: responseData.userId,
      roles: responseData.roles,
    });

    console.log(responseData);
  },

  tryLogin(context) {
    const userId = localStorage.getItem("userId");
    const userRoles = localStorage.getItem("userRoles");
    const tokenExpiration = localStorage.getItem("tokenExpiration");

    const expiresIn = +tokenExpiration - new Date().getTime();
    if (expiresIn < 0) {
      return;
    }

    timer = setTimeout(function () {
      context.dispatch("autoLogout");
    }, expiresIn);

    context.commit("setUser", {
      userId: userId,
      roles: userRoles.split(","),
    });
  },

  logout(context) {
    localStorage.removeItem("userId");
    localStorage.removeItem("tokenExpiration");
    localStorage.removeItem("userRoles");

    clearTimeout(timer);

    context.commit("setUser", {
      userId: null,
      roles: null,
    });

    context.commit("setAutoLogout");
  },

  autoLogout(context) {
    context.dispatch("logout");
    context.commit("setAutoLogout");
  },
};
