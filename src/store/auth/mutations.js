export default {
    setUser(state, payload) {
        state.roles = [];
        // state.token = payload.token;
        state.userId = payload.userId;
        payload.roles !== null ? state.roles.push(...payload.roles) : null;
        state.didAutoLogout = false;
    },

    setAutoLogout(state) {
        state.didAutoLogout = true;
    }
}